package com.citi.training.stocks.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.stocks.dao.StockDao;
import com.citi.training.stocks.model.Stock;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
public class StockControllerTests {

    private static final Logger logger = LoggerFactory.getLogger(StockControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockDao stockDao;

    @Test
    public void findAllStocks_returnsList() throws Exception {
        when(stockDao.findAll()).thenReturn(new ArrayList<Stock>());

        MvcResult result = this.mockMvc.perform(get("/stocks")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from StockDao.findAll: " +
                    result.getResponse().getContentAsString());
    }


    @Test
    public void getStockById_returnsOK() throws Exception {
        Stock testStock = new Stock(5, "AAPL", "Apple Computers Inc.");

        when(stockDao.findById(testStock.getId())).thenReturn(
                                                    Optional.of(testStock));

        MvcResult result = this.mockMvc.perform(get("/stocks/" + testStock.getId()))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from StockDao.getStock: " +
                    result.getResponse().getContentAsString());
    }

}
