package com.citi.training.stocks.rest;

import org.junit.Test;

public class EqualsTests {
	
	@Test
	public void test_equals(){
		
		String firstString = "A_String";
		String secondString = "A_String";
		
		String clonedFirstString = new String(firstString);
		
		System.out.println("First: " + firstString );
		System.out.println("Second: " + secondString );
		System.out.println("Cloned: " + clonedFirstString );
		
		assert(firstString.equals(secondString));
		assert(firstString == secondString);
		assert(clonedFirstString == firstString);
	}

}
